import NotificationsContainer from './NotificationsContainer.vue';
import NotificationContent from './NotificationContent.vue';

export { NotificationContent, NotificationsContainer };
