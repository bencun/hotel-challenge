/**
 * Injects metadata related to authentication and roles into the routes
 */
function RequiresAuth(routes, redirectRoute) {
  return routes.map((r) => {
    const rObj = { ...r };
    rObj.meta = {
      requiresAuth: true,
      redirectRoute,
    };
    return rObj;
  });
}


export default RequiresAuth;
