import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '@/store';
import { AuthModuleName } from '@/store/AuthModule';

import MainContainer from '@/MainContainer.vue';

import AuthRoutes, { loginRouteName } from '@/sections/Auth';
import HotelsRoutes from '@/sections/Hotels';
import DetailsView from '@/sections/Hotels/views/DetailsView.vue';
import RequiresAuth from './authMetaInjector';


Vue.use(VueRouter);

const loginRoute = { name: loginRouteName };

const routes = [
  {
    name: 'Root',
    path: '/',
    component: MainContainer,
    redirect: loginRoute,
    children: [
      ...AuthRoutes,
      ...RequiresAuth(HotelsRoutes, loginRoute),
      ...RequiresAuth([
        {
          name: 'Details',
          path: 'details/:id',
          props: (route) => ({ id: parseInt(route.params.id, 10) }),
          component: DetailsView,
        },
      ]),
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const shouldAuth = to.matched.some((r) => r.meta.requiresAuth);
  const loginIsNext = to.matched.some((r) => r.name === loginRouteName);
  const authenticated = store.state[AuthModuleName].loggedIn;
  let finalRoute;

  // if logged in then skip login
  if (loginIsNext && authenticated) {
    finalRoute = { name: 'Hotels' };
  }
  // if route requires auth
  if (shouldAuth) {
    console.log('This route requires auth');
    const lastRoute = to.matched.slice().reverse().find((r) => r.meta.requiresAuth);
    // console.log('guard state', to.matched, shouldAuth, authenticated, lastRoute);
    if (!authenticated) {
      console.log('This route is not authenticated');
      finalRoute = lastRoute.meta.redirectRoute;
    }
  }
  next(finalRoute);
});

export default router;
