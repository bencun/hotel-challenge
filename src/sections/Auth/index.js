import LoginView from './views/LoginView.vue';
import RegisterView from './views/RegisterView.vue';

import loginRouteName from './loginRouteName';

export { loginRouteName };

export default [
  {
    name: loginRouteName,
    path: 'login',
    component: LoginView,
  },
  {
    name: 'Register',
    path: 'register',
    component: RegisterView,
  },
];
