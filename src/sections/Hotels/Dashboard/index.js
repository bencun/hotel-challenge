import DashboardView from './views/DashboardView.vue';

export default [
  {
    name: 'Dashboard',
    path: 'dashboard',
    component: DashboardView,
  },
];
