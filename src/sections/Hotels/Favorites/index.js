import FavoritesView from './views/FavoritesView.vue';

export default [
  {
    name: 'Favorites',
    path: 'favorites',
    component: FavoritesView,
  },
];
