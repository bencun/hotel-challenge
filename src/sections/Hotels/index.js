import HotelsView from './views/HotelsView.vue';
import Dashboard from './Dashboard';
import Favorites from './Favorites';

export default [
  {
    name: 'Hotels',
    path: 'hotels',
    component: HotelsView,
    redirect: { name: Dashboard[0].name },
    children: [
      ...Dashboard,
      ...Favorites,
    ],
  },
];
