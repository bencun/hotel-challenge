import http from './http';

const endpoints = Object.freeze({
  login: '/api-token-auth/',
  register: '/register/',
});

export default {
  Authenticate(params) {
    return http.post(endpoints.login, params)
      .then((r) => r.data)
      .catch((r) => { throw r.data; });
  },
  Register(params) {
    return http.post(endpoints.register, params)
      .then((r) => r.data)
      .catch((r) => { throw r.data; });
  },
};
