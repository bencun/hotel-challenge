import http from './http';

const hotelApi = '/hotel_api/';
const favoritesApi = '/favorites/';

const endpoints = Object.freeze({
  hotelReviews: `${hotelApi}get_hotel_reviews/`,
  favoritesAddRemove: `${favoritesApi}add_remove`,
});

export default {
  getAllHotels() {
    return http.get(hotelApi)
      .then((r) => r.data)
      .catch((e) => { throw e.data; });
  },
  getHotelDetails(id) {
    return http.get(`${hotelApi}${id}/`)
      .then((r) => r.data)
      .catch((e) => { throw e.data; });
  },
  getHotelReviews(id) {
    return http.get(`${endpoints.hotelReviews}${id}/`)
      .then((r) => r.data)
      .catch((e) => { throw e.data; });
  },
  getFavorites() {
    return http.get(favoritesApi)
      .then((r) => r.data)
      .catch((e) => { throw e.data; });
  },
  // eslint-disable-next-line camelcase
  addRemoveFavorite({ hotel_id, is_favorite }) {
    const fd = new FormData();
    fd.append('hotel_id', hotel_id);
    fd.append('is_favorite', is_favorite);

    const reqOpt = {
      headers: { 'Content-Type': 'multipart/form-data' },
    };

    return http.post(endpoints.favoritesAddRemove, fd, reqOpt)
      .then((r) => r.data)
      .catch((e) => { throw e.data; });
  },
};
