import axios from 'axios';
// import store from '@/store';
// import { AuthModuleName } from '@/store/AuthModule';

// store registration hook to avoid circular dep
let store = null;
let authModuleName = null;

export const registerStore = (storeRef, authModuleNameRef) => {
  store = storeRef;
  authModuleName = authModuleNameRef;
};

// set-up the axios instance with the interceptor for auth
const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  (config) => {
    const newConfig = { ...config };
    if (store && authModuleName) {
      const { token } = store.state[authModuleName];
      if (token) {
        newConfig.headers.Authorization = `Token ${token}`;
      }
    }
    return newConfig;
  },
  (error) => Promise.reject(error),
);

axiosInstance.interceptors.response.use((response) => Promise.resolve(response), (error) => {
  // unauthorized or error
  console.log('Request failed', error.response.status);
  if (error.response.status === 401) {
    // invalidate login
  }

  return Promise.reject(error.response);
});

export default axiosInstance;
