import AuthService from '@/services/auth';

export const AuthModuleName = 'Auth';
export const AuthActions = Object.freeze({
  startLogin: 'startLogin',
  startRegister: 'startRegister',
  logout: 'logout',
  checkLogin: 'checkLogin',
});
const AuthMutations = Object.freeze({
  saveLoginInfo: 'saveLoginInfo',
  clearLoginInfo: 'clearLoginInfo',
});

const LocalStorageUserKey = 'user_info';


export default {
  namespaced: true,
  state: {
    loggedIn: false,
    token: null,
    loginInfo: null,
  },
  actions: {
    [AuthActions.startLogin](context, params) {
      return AuthService.Authenticate(params)
        .then((data) => {
          context.commit(AuthMutations.saveLoginInfo, data);
        })
        .catch((e) => { throw e; });
    },
    [AuthActions.startRegister](context, params) {
      return AuthService.Register(params)
        .then((data) => data)
        .catch((e) => { throw e; });
    },
    [AuthActions.logout]({ commit }) {
      commit(AuthMutations.clearLoginInfo);
    },
    [AuthActions.checkLogin]({ commit, state }) {
      if (state.loggedIn) return true;

      const userDataLS = JSON.parse(localStorage.getItem(LocalStorageUserKey));
      if (userDataLS && userDataLS.token) {
        commit(AuthMutations.saveLoginInfo, userDataLS);
        return true;
      }

      return false;
    },
  },
  mutations: {
    [AuthMutations.saveLoginInfo](state, loginData) {
      state.loggedIn = true;
      state.loginInfo = loginData;
      state.token = loginData.token;

      localStorage.setItem(LocalStorageUserKey, JSON.stringify(loginData));
    },
    [AuthMutations.clearLoginInfo](state) {
      state.loggedIn = false;
      state.loginInfo = null;
      state.token = null;

      localStorage.removeItem(LocalStorageUserKey);
    },
  },
  getters: {},
};
