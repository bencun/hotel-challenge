import HotelsService from '@/services/hotels';

export const HotelsModuleName = 'Hotels';
export const HotelsActions = Object.freeze({
  getAllHotels: 'getAllHotels',
  toggleFavorite: 'toggleFavorite',
});
const HotelsMutations = Object.freeze({
  saveAllHotels: 'saveAllHotels',
  saveFavorites: 'saveFavorites',
  setFavForHotel: 'setFavForHotel',
});

export const HotelsGetters = Object.freeze({
  isFavorite: 'isFavorite',
});

export default {
  namespaced: true,
  state: {
    hotels: [],
    favorites: [],
    hotelDetailed: null,
  },
  actions: {
    [HotelsActions.getAllHotels]({ commit }) {
      const allHotels = HotelsService.getAllHotels()
        .then((hotels) => commit(HotelsMutations.saveAllHotels, hotels))
        .catch((e) => { throw e; });
      const favs = HotelsService.getFavorites()
        .then((hotels) => commit(HotelsMutations.saveFavorites, hotels))
        .catch((e) => { throw e; });
      return Promise.all([allHotels, favs]);
    },
    [HotelsActions.getFavorites]({ commit }) {
      return HotelsService.getFavorites()
        .then((hotels) => commit(HotelsMutations.saveFavorites, hotels))
        .catch((e) => { throw e; });
    },
    // toggle favorites
    [HotelsActions.toggleFavorite]({ commit, getters }, hotel) {
      // remove the reactivity
      const hotelObject = JSON.parse(JSON.stringify(hotel));
      let favValue = true;
      if (getters[HotelsGetters.isFavorite](hotelObject.id)) {
        favValue = false;
      }
      console.log('hotel fav', favValue);
      return HotelsService.addRemoveFavorite({ hotel_id: hotelObject.id, is_favorite: favValue })
        .then(() => commit(HotelsMutations.setFavForHotel, { hotel: hotelObject, favValue }))
        .catch((e) => { throw e; });
    },
  },
  mutations: {
    [HotelsMutations.saveAllHotels](state, hotels) {
      state.hotels = hotels;
    },
    [HotelsMutations.saveFavorites](state, hotels) {
      state.favorites = hotels;
    },
    [HotelsMutations.setFavForHotel](state, { hotel, favValue }) {
      // try to find the hotel in favorites
      const hotelIdx = state.favorites.findIndex((h) => h.id === hotel.id);

      if (hotelIdx >= 0) {
        if (favValue) return;
        state.favorites.splice(hotelIdx, 1);
      } else if (favValue) {
        state.favorites.push(hotel);
      }
    },
  },
  getters: {
    [HotelsGetters.isFavorite](state) {
      return (hid) => state.favorites.some(((h) => h.id === hid));
    },
  },
};
