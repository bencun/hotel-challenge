export const NotificationsModuleName = 'Notifications';
export const NotificationActions = Object.freeze({
  addNtf: 'addNtf',
  closeNtf: 'closeNtf',
});
const NotificationMutations = Object.freeze({
  pushNtf: 'pushNtf',
  spliceNtf: 'spliceNtf',
});

export const ntfTypes = Object.freeze({
  info: 'info',
  error: 'error',
});
const defaultNtfParams = {
  timeout: 5,
  type: ntfTypes.info,
  content: '',
};


export default {
  namespaced: true,
  state: {
    notifications: [],
  },
  actions: {
    [NotificationActions.addNtf]({ commit }, ntfConfig) {
      const ntfcObj = {
        ...defaultNtfParams,
        ...ntfConfig,
      };

      // timeout
      ntfcObj.timeoutRef = setTimeout(() => {
        commit(NotificationMutations.spliceNtf, ntfcObj);
      }, ntfcObj.timeout * 1000);

      // push to array
      commit(NotificationMutations.pushNtf, ntfcObj);

      return ntfcObj;
    },
  },
  mutations: {
    [NotificationMutations.pushNtf](state, ntfcObj) {
      state.notifications.push(ntfcObj);
    },
    [NotificationMutations.spliceNtf](state, ntfcObj) {
      const idx = state.notifications.indexOf(ntfcObj);
      state.notifications.splice(idx, 1);
    },
  },
  getters: {},
};
