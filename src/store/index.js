import Vue from 'vue';
import Vuex from 'vuex';
import { registerStore } from '@/services/http';

import AuthModule, { AuthModuleName, AuthActions } from './AuthModule';
import NotificationsModule, { NotificationsModuleName } from './NotificationsModule';
import HotelsModule, { HotelsModuleName } from './HotelsModule';

Vue.use(Vuex);

const appStore = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  },
  modules: {
    [AuthModuleName]: AuthModule,
    [NotificationsModuleName]: NotificationsModule,
    [HotelsModuleName]: HotelsModule,
  },
});

registerStore(appStore, AuthModuleName);
appStore.dispatch(`${AuthModuleName}/${AuthActions.checkLogin}`);

export default appStore;
