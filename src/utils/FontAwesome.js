import Vue from 'vue';
// import the Vue component
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// import the icon library
import { library } from '@fortawesome/fontawesome-svg-core';
// import the that icons to be used GLOBALLY
// IF YOU DON'T NEED AN ICON AVAILABLE GLOBALLY THEN IMPORT IT INSIDE OF YOUR COMPONENT
import {
  faSpinner,
  faHeart as fasHeart,
  faStar as fasStar,
  faAngleDown,
  faAngleUp,
  faThumbsDown,
  faThumbsUp,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import {
  faStar as farStar,
  faHeart as farHeart,
} from '@fortawesome/free-regular-svg-icons';

// add the icons to the GLOBAL library of icons
library.add(
  faSpinner,
  fasHeart,
  farHeart,
  fasStar,
  farStar,
  faAngleDown,
  faAngleUp,
  faThumbsDown,
  faThumbsUp,
  faUser,
);

// create the Vue component itself
Vue.component('fa-icon', FontAwesomeIcon);
