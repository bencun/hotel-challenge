export default function ParseDate(dateAsString) {
  const [date] = dateAsString.split(' ');
  const [y, m, d] = date.split('-');

  return `${d}/${m}/${y}`;
}
