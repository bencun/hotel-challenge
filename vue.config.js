// const path = require('path');

console.info('Custom config loaded.', process.env.VUE_APP_DOMAIN);

module.exports = {
  configureWebpack: {
    devtool: 'source-map',
    /* resolve: {
      alias: {
        'less-vars': path.join(__dirname, '/src/less/less-variables.less'),
      },
    }, */
  },
  devServer: {
    proxy: `http://${process.env.VUE_APP_DOMAIN}/`,
  },
};
